#!/bin/bash

# Add an entry in truenas' init scripts (post init) with the auto argument (e.g. /root/it87_fan_fix.sh auto)
# This should survive any updates that are released, by building and reinstalling anytime something goes missing.
# If there are ever any updates to the fabulous drivers provided by mafredri they too can be picked up either on the 
# next boot, or by simply re-running this script again in the shell

# Wait for Internet
while ! ping -c 1 google.com; do
  echo "Waiting for Internet ..."
  sleep 5 
done

# Make sure the module is installed and Up To date
echo "Checking for module"
mkdir -p /root/asustor-platform-driver; cd /root/asustor-platform-driver
if ! ls -l /sys/class/hwmon | grep -qsi asustor_it87 || ! git pull | grep 'Already up to date.'; then
  if ! ls -l /sys/class/hwmon | grep -qsi asustor_it87; then
	echo "Module not installed"
  else
  	echo "Module Update available"
	rmmod asustor_it87
  fi
  install-dev-tools > /dev/null 2>&1
  apt install -yqq smartmontools gcc-12 golang
  if [[ -d /root/asustor-platform-driver/.git ]]; then
    cd /root/asustor-platform-driver
    git pull -q 
  else
    cd /root
    git clone -q https://github.com/mafredri/asustor-platform-driver
    cd asustor-platform-driver
  fi
  make clean > /dev/null 2>&1
  echo "Building Module"
  make > /dev/null
  echo "Installing Module"
  make install > /dev/null

  echo "Starting Module"
  depmod -a
  modprobe -a asustor_it87

  echo "Auto Configuring sensor settings"
  sensors-detect --auto > /dev/null 2>&1
fi

# Find loaded Module Location
hwmon_it87="/sys/class/hwmon/"`ls -lQ /sys/class/hwmon | grep -i asustor_it87 | cut -d "\"" -f 2`

# Make The fans loud to verify if the control worked.
if [[ ! $1 == "auto" ]]; then
  #Set fans to loud (255 max)
  echo 200 > ${hwmon_it87}/pwm1
  cat ${hwmon_it87}/pwm1
  echo -e "\n\n#########################################################\n      You should hear the fan at full speed now\n#########################################################\n\n"

  read -s -n1 -p "Did the fans startup? [y/n] " fans_working 
else
  fans_working="y"
fi

if [[ "${fans_working,,}" == "y" ]]; then
  # make main HD light stop Blinking (0=solid)
  echo 0 > "${hwmon_it87}"/gpled1_blink
  # Set left side lights brightness (0=bright 250=low 255=off)
  echo 245 > "${hwmon_it87}"/pwm3

  # Install modules to start after boot
  echo -e "\nOk great, installing the modules to start at boot\n"
  if ! grep -qs ^asustor_it87 /etc/modules; then 
    echo -e "# load custom it87 module\nasustor_it87" >> /etc/modules
    echo "Module Installation Complete"
  else
    echo "Already Setup to start at boot"
  fi
else
  echo -e "\n\nSomething went wrong with the module\n\n"
  exit 3
fi

echo "Installing the fan control script"
wget -q https://raw.githubusercontent.com/bernmc/flashstor-trueNAS-fancontrol/main/temp_monitor.sh -O /root/temp_monitor.sh > /dev/null 2>&1
chmod +x /root/temp_monitor.sh
# Set temp check frequency to 5 seconds
sed -i "s/^frequency=.*/frequency=5/g" /root/temp_monitor.sh
# Set Minimum fan speed
sed -i "s/^min_pwm=.*/min_pwm=80/g" /root/temp_monitor.sh
# Turn off Mail alerts
sed -i "s/^mailalerts=.*/mailalerts=0/g" /root/temp_monitor.sh

# Set script to run at boot
if [[ ! -f /etc/rc.local ]]; then 
    touch /etc/rc.local
    echo -e "#!/bin/sh -e\n#launch custom fan control script\nnohup /root/temp_monitor.sh &" >> /etc/rc.local
fi
if ! grep -qs "^nohup /root/temp_monitor.sh &" /etc/rc.local; then 
    echo -e "#launch custom fan control script\nnohup /root/temp_monitor.sh &" >> /etc/rc.local
fi
chmod +x /etc/rc.local 

# make sure the temp monitor script isn't already running before starting
if ! ps -a | grep -qs /root/temp_monitor.sh; then
  /root/temp_monitor.sh > /dev/null 2>&1 &
fi

# LCD controller
mkdir -p /root/lcm; cd /root/lcm
echo "Installing LCD controller"
if ! systemctl status openlcmd | grep -qs running || ! git pull | grep 'Already up to date.'; then
  if ! which go; then
	install-dev-tools
	apt-get install -qq golang
  fi
  if [[ -d /root/lcm/.git ]]; then
    cd /root/lcm
    git pull -q
  else
    cd /root
    git clone -q https://github.com/mafredri/lcm
    cd lcm
  fi
  # golang needs this to setup an env while not in a proper tty (ie at boot)
  export HOME="/root"
  go build ./cmd/openlcmd
  cp openlcmd /usr/local/sbin/openlcmd
  cp cmd/openlcmd/systemd/openlcmd.service /etc/systemd/system/openlcmd.service
  systemctl enable openlcmd
  systemctl restart openlcmd
fi

if [[ ! $1 == "auto" ]]; then
  echo -e "\n\n############################################################\n   Unless the system is really hot the fans should sound\n   more normal now, a reboot is suggested to make\n   sure everything works after a reboot\n############################################################\n\n"

  read -s -n1 -p "Reboot Now? [y/n]  " reboot_now
  if [[ "${reboot_now,,}" == "y" ]]; then
    reboot
  fi
fi
